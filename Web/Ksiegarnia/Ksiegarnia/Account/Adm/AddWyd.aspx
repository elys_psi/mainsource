﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddWyd.aspx.cs" Inherits="Ksiegarnia.Account.AddWyd" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div>
            <div class="form-horizontal" runat="server" id="RegisterForm">
        <h4>Dodaj wydawnictwo</h4>
        <hr />

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Nazwa Wydawnictwa</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-2 control-label">Miasto</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="City" CssClass="form-control" />
            </div>
        </div>


        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Code" CssClass="col-md-2 control-label">Kod Pocztowy</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Code" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Uwags" CssClass="col-md-2 control-label">Uwagi</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Uwags" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="Create" Text="Dodaj wydawnictwo" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</div>
</asp:Content>
