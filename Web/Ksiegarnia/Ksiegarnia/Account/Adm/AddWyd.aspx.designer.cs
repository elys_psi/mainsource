﻿//------------------------------------------------------------------------------
// <generowany automatycznie>
//     Ten kod został wygenerowany przez narzędzie.
//
//     Modyfikacje tego pliku mogą spowodować niewłaściwe zachowanie i zostaną utracone
//     w przypadku ponownego wygenerowania kodu. 
// </generowany automatycznie>
//------------------------------------------------------------------------------

namespace Ksiegarnia.Account {
    
    
    public partial class AddWyd {
        
        /// <summary>
        /// Kontrolka RegisterForm.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl RegisterForm;
        
        /// <summary>
        /// Kontrolka Name.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Name;
        
        /// <summary>
        /// Kontrolka City.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox City;
        
        /// <summary>
        /// Kontrolka Code.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Code;
        
        /// <summary>
        /// Kontrolka Uwags.
        /// </summary>
        /// <remarks>
        /// Pole generowane automatycznie.
        /// By zmodyfikować deklaracje pola przenieś ją z pliku projektanta do pliku codebehind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Uwags;
    }
}
