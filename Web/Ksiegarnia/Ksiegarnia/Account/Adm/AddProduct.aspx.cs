﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;
using Ksiegarnia.Models.StaticQuerys;

namespace Ksiegarnia.Account
{
    public partial class AddProduct : System.Web.UI.Page
    {
        protected void Page_Load()
        {
            var sessionId = Session["User"];
            if(sessionId == null)
            {
                Response.Redirect("/Default");
            }
            var id = Rol.Current(Session["User"].ToString());
            if(id != 2)
            {
                Response.Redirect("/Default");
            }
        }

        protected void Create(object sender, EventArgs e)
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var book = new Ksiazki();
                book.Tytul = Name.Text;
                book.ISBN = Convert.ToDecimal(ISBN.Text);
                book.Oprawa = Oprawa.Text;
                book.OpisKsi = Description.Text;
                book.MiejsceWydania = City.Text;
                book.StanMagazynowy = Convert.ToInt32(Count.Text);
                book.Cena = Convert.ToInt32(Price.Text);
                book.RokWydania = DateTime.ParseExact(Year.Text, "yyyy", System.Globalization.CultureInfo.InvariantCulture);
                book.IdAutora = Convert.ToInt32(DropDownList1.SelectedValue);
                book.IdWydawnictwa = Convert.ToInt32(DropDownList2.SelectedValue);
                book.IdKategorii = Convert.ToInt32(DropDownList3.SelectedValue);

                ctx.Ksiazki.Add(book);

                ctx.SaveChanges();

            }
        }

    }
}