﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddAuthor.aspx.cs" Inherits="Ksiegarnia.Account.AddAuthor" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div>
            <div class="form-horizontal" runat="server" id="RegisterForm">
        <h4>Dodaj autora</h4>
        <hr />

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Imię</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Surname" CssClass="col-md-2 control-label">Nazwisko</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Surname" CssClass="form-control" />
            </div>
        </div>


        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Titles" CssClass="col-md-2 control-label">Tytuł naukowy</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Titles" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Uwags" CssClass="col-md-2 control-label">Uwagi</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Uwags" CssClass="form-control" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="Create" Text="Register" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</div>
</asp:Content>
