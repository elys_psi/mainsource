﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;
using Ksiegarnia.Models.StaticQuerys;

namespace Ksiegarnia.Account
{
    public partial class AddAuthor : System.Web.UI.Page
    {
        protected void Page_Load()
        {
            var sessionId = Session["User"];
            if(sessionId == null)
            {
                Response.Redirect("/Default");
            }
            var id = Rol.Current(Session["User"].ToString());
            if(id != 2)
            {
                Response.Redirect("/Default");
            }
        }

        protected void Create(object sender, EventArgs e)
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var autor = new Autorzy();
                autor.ImieAut = Name.Text;
                autor.NazwiskoAut = Surname.Text;
                autor.TytulyNaukowe = Titles.Text;
                autor.UwagiAut = Uwags.Text;
                ctx.Autorzy.Add(autor);

                ctx.SaveChanges();
            }
        }
    }
}