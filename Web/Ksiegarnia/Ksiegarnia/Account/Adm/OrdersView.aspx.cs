﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;
using Ksiegarnia.Models.StaticQuerys;

namespace Ksiegarnia.Account
{
    public partial class OrdersView : System.Web.UI.Page
    {
        protected void Page_Load()
        {
            var sessionId = Session["User"];
            if(sessionId == null)
            {
                Response.Redirect("/Default");
            }
            var id = Rol.Current(Session["User"].ToString());
            if(id != 2)
            {
                Response.Redirect("/Default");
            }

            using (var ctx = new ksiegarniaEntities())
            {
                var list =
                    from z in ctx.Zamowienia
                    join u in ctx.Osoby on z.IdUzytkownika equals u.IdUzytkownika
                    select new { Id = z.IdZamowienia, User = u.C_Login, Date = z.DataZam, Status = z.StatusZam };

                DataTable dt = new DataTable();


                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Uzytkownik", typeof(string));
                dt.Columns.Add("Data", typeof(DateTime));
                dt.Columns.Add("Status", typeof(string));
                dt.Columns.Add("Szczegoly", typeof(string));
                var orders = list.AsEnumerable();
                foreach (var order in orders)
                {
                    dt.Rows.Add(order.Id, order.User, order.Date, order.Status,"Szczegóły");
                }
                OrdersGrid.DataSource = dt;
                OrdersGrid.DataBind();
            }

        }

        protected void OrdersGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //linkowanie linków do kazdego pola
                HyperLink newHyperLink = new HyperLink();
                newHyperLink.Text = e.Row.Cells[4].Text;
                newHyperLink.NavigateUrl = "OrderDetails?orderId=" + e.Row.Cells[0].Text;
                e.Row.Cells[4].Controls.Add(newHyperLink);
            }
        }

        protected void Create(object sender, EventArgs e)
        {

        }
    }
}