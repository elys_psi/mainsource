﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReplyComplaint.aspx.cs" Inherits="Ksiegarnia.Account.ReplyComplaint" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <br />
        <br />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ReplyType" CssClass="col-md-2 control-label">Typ odpowiedzi</asp:Label>
            <div class="col-md-10">
                    <asp:DropDownList ID="ReplyType" runat="server" CssClass="btn btn-primary dropdown-toggle">
                    </asp:DropDownList>
            </div>
        </div>
        <br />
        <br />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Description" CssClass="col-md-2 control-label">Odpowiedź</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Description" CssClass="form-control" />
            </div>
        </div>
        <br />
        <br />
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="Reply" Text="Odpowiedz" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</asp:Content>
