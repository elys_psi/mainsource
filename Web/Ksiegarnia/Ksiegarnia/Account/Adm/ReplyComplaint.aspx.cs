﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;
using Ksiegarnia.Models.StaticQuerys;

namespace Ksiegarnia.Account
{
    public partial class ReplyComplaint : System.Web.UI.Page
    {
        protected void Page_Load()
        {
            var sessionId = Session["User"];

            if(sessionId == null)
            {
                Response.Redirect("/Default");
            }
            var id = Rol.Current(Session["User"].ToString());

            if (id != 2)
            {
                Response.Redirect("/Default");
            }

            DropDownInit();
        }

        protected void Reply(object sender, EventArgs e)
        {
            int complaintId = Convert.ToInt32(Request.QueryString["ComplaintId"]);
            using (var ctx = new ksiegarniaEntities())
            {
                var complaint = ctx.ReklamacjeZamowien.FirstOrDefault(r => r.IdZamowienia == complaintId);

                if (complaint != null)
                {
                    complaint.DecyzjaRek = Description.Text;
                    complaint.StanRek = ReplyType.Text;
                    ctx.SaveChanges();
                }

                Response.Redirect("/Account/Complaints");
            }
        }
        private void DropDownInit()
        {
            ReplyType.Items.Add("Reklamacja uznana");
            ReplyType.Items.Add("Odmowa");
        }
    }
}