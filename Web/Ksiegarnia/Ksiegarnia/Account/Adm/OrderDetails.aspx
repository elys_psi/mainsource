﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrderDetails.aspx.cs" Inherits="Ksiegarnia.OrderDetails" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h2> Zamówienie
        </h2>
    </div>

    <div>
        <asp:GridView ID="OrderGrid" runat="server" CssClass ="table table-striped" onrowdatabound="OrderGrid_RowDataBound" >
        </asp:GridView>
    </div>
    <br /><br /><br />
    <div>
        <asp:GridView ID="DetailsGrid" runat="server" CssClass ="table table-striped">
            </asp:GridView>
    </div>
</asp:Content>
