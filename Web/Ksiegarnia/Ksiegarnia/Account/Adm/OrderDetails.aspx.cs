﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Web.UI.WebControls;
using System.Data;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;
using Ksiegarnia.Models.StaticQuerys;

namespace Ksiegarnia
{
    public partial class OrderDetails : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.QueryString["realized"] == "1") && (Request.QueryString["orderId"] != null))
            {
                ChangeStatus(Convert.ToInt32(Request.QueryString["orderId"]));
            }
            //jesli user nie jest zalogowany idzie do login page i daje mu parametr powrotu na ta strone.
            if(Session["User"] == null)
            {
                Response.Redirect("Account/Login?back=Order");
            }


            //weryfikacja roli admina
            var id = Rol.Current(Session["User"].ToString());
            if (id != 2)
            {
                Response.Redirect("/Default");
            }


            var orderId = Request.QueryString["orderId"];
            if (orderId != null)
            {
                var oId = Convert.ToInt32(orderId);
                using (var ctx = new ksiegarniaEntities())
                {

                    //Generacja tabeli zamówienia
                    var order =
                        from z in ctx.Zamowienia
                        join u in ctx.Osoby on z.IdUzytkownika equals u.IdUzytkownika
                        where z.IdZamowienia == oId
                        select new { Id = z.IdZamowienia, User = u.C_Login, Date = z.DataZam, Status = z.StatusZam, Discount = z.Rabat };

                    DataTable dt = new DataTable();
                    dt.Columns.Add("Id", typeof(int));
                    dt.Columns.Add("Uzytkownik", typeof(string));
                    dt.Columns.Add("Data", typeof(DateTime));
                    dt.Columns.Add("Rabat", typeof(string));
                    dt.Columns.Add("Status", typeof(string));
                    dt.Columns.Add("Oznacz jako zrealizowane", typeof(string));
                    var orders = order.ToList();
                    dt.Rows.Add(orders[0].Id, orders[0].User, orders[0].Date, orders[0].Discount + "%", orders[0].Status, "Zatwierdź realizację");
                    OrderGrid.DataSource = dt;
                    OrderGrid.DataBind();



                    //Generacja produktów zamówienia

                    //var list =
                    //    from z in ctx.ZbiorZamowienia
                    //    join b in ctx.Ksiazki on z.ISBN equals b.ISBN
                    //    join a in ctx.Autorzy on b.IdAutora equals a.IdAutora
                    //    where z.IdZamowienia == oId
                    //    select new KoszykElement { ISBN = b.ISBN, Title = b.Tytul, Description = b.OpisKsi, Year = b.RokWydania, Ilosc = z.IloscKsiazek, Autor = a.NazwiskoAut + " " + a.ImieAut };

                    //DataTable dt2 = new DataTable();
                    //dt2.Columns.Add("Ksiazka", typeof(string));
                    //dt2.Columns.Add("Autor", typeof(string));
                    //dt2.Columns.Add("Opis", typeof(string));
                    //dt2.Columns.Add("Ilosc", typeof(int));
                    //DetailsGrid.DataSource = dt2;
                    //var orders2 = list.AsEnumerable();
                    //foreach (var book in orders2)
                    //{
                    //    dt2.Rows.Add(book.Title, book.Autor, book.Description, book.Ilosc);
                    //}
                    DetailsGrid.DataBind();
                }
            }
             
        }

        protected void OrderGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //linkowanie linków do kazdego pola
                HyperLink newHyperLink = new HyperLink();
                newHyperLink.Text = e.Row.Cells[5].Text;
                newHyperLink.NavigateUrl = "OrderDetails?orderId=" + e.Row.Cells[0].Text + "&realized=1";
                e.Row.Cells[5].Controls.Add(newHyperLink);
            }
        }


        private void ChangeStatus(int orderId)
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var order = ctx.Zamowienia.FirstOrDefault(z => z.IdZamowienia == orderId);
                if (order != null)
                {
                    order.StatusZam = "Zrealizowane";
                    ctx.SaveChanges();
                }
            }
        }
    }
}