﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddProduct.aspx.cs" Inherits="Ksiegarnia.Account.AddProduct" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="form-horizontal" runat="server" id="RegisterForm">
            <h4>Dodaj książkę</h4>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Name" CssClass="col-md-2 control-label">Nazwa książki</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Name" CssClass="form-control" />
                </div>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="DropDownList1" CssClass="col-md-2 control-label">Autor</asp:Label>
                <div class="col-md-10">
                     <asp:DropDownList ID="DropDownList1" runat="server" CssClass="btn btn-primary dropdown-toggle" DataSourceID="EntityDataSource1" DataTextField="NazwiskoAut" DataValueField="IdAutora">
                     </asp:DropDownList>
                     <asp:EntityDataSource ID="EntityDataSource1" runat="server" ConnectionString="name=ksiegarniaEntities" DefaultContainerName="ksiegarniaEntities" EnableFlattening="False" EntitySetName="Autorzy" EntityTypeFilter="Autorzy" Select="it.[IdAutora], it.[NazwiskoAut]">
                     </asp:EntityDataSource>
                </div>
            </div>
            
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="DropDownList2" CssClass="col-md-2 control-label">Wydawnictwo</asp:Label>
                <div class="col-md-10">
                     <asp:DropDownList ID="DropDownList2" runat="server" CssClass="btn btn-primary dropdown-toggle" DataSourceID="EntityDataSource2" DataTextField="NazwaWyd" DataValueField="IdWydawnictwa">
                     </asp:DropDownList>
                     <asp:EntityDataSource ID="EntityDataSource2" runat="server" ConnectionString="name=ksiegarniaEntities" DefaultContainerName="ksiegarniaEntities" EnableFlattening="False" EntitySetName="Wydawnictwa" Select="it.[IdWydawnictwa], it.[NazwaWyd]">
                     </asp:EntityDataSource>
                </div>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="ISBN" CssClass="col-md-2 control-label">ISBN</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="ISBN" CssClass="form-control" />
                </div>
            </div>
            
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Price" CssClass="col-md-2 control-label">Cena</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Price" CssClass="form-control" />
                </div>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Description" CssClass="col-md-2 control-label">Opis</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Description" CssClass="form-control" />
                </div>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="DropDownList3" CssClass="col-md-2 control-label">Kategoria</asp:Label>
                <div class="col-md-10">
                     <asp:DropDownList ID="DropDownList3" runat="server" CssClass="btn btn-primary dropdown-toggle" DataSourceID="EntityDataSource3" DataTextField="NazwaKat" DataValueField="IdKategorii">
                     </asp:DropDownList>
                    <asp:EntityDataSource ID="EntityDataSource3" runat="server" ConnectionString="name=ksiegarniaEntities" DefaultContainerName="ksiegarniaEntities" EnableFlattening="False" EntitySetName="Kategorie" EntityTypeFilter="Kategorie" Select="it.[IdKategorii], it.[NazwaKat]">
                    </asp:EntityDataSource>
                </div>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Oprawa" CssClass="col-md-2 control-label">Oprawa</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Oprawa" CssClass="form-control" />
                </div>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Year" CssClass="col-md-2 control-label">Rok wydania</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Year" CssClass="form-control" />
                </div>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Count" CssClass="col-md-2 control-label">Stan magazynowy</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="Count" CssClass="form-control" />
                </div>
            </div>

            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="City" CssClass="col-md-2 control-label">Miejsce wydania</asp:Label>
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="City" CssClass="form-control" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="Create" Text="Dodaj wydawnictwo" CssClass="btn btn-default" />
                </div>
            </div>



        </div>
    </div>
</asp:Content>
