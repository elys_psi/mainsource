﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;
using Ksiegarnia.Models.StaticQuerys;

namespace Ksiegarnia.Account
{
    public partial class AddCategory : System.Web.UI.Page
    {
        protected void Page_Load()
        {
            var sessionId = Session["User"];
            if(sessionId == null)
            {
                Response.Redirect("/Default");
            }
            var id = Rol.Current(Session["User"].ToString());
            if(id != 2)
            {
                Response.Redirect("/Default");
            }
        }

        protected void Create(object sender, EventArgs e)
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var category = new Kategorie();
                category.NazwaKat = Name.Text;
                category.OpisKat = Description.Text;

                ctx.SaveChanges();
            }
        }
    }
}