﻿using System;
using System.Web;
using System.Web.UI;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;

namespace Ksiegarnia.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {        

            //wylogowanie (kiedy user wejdzie na login page i jest zalogowany, nastepuje wylogowanie
            if(Session["User"]  != null)
            {
                Session["User"] = null;
                Response.Redirect("Login.aspx");
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {
            //logowanie
            using (var ctx = new ksiegarniaEntities())
            {
                var user = ctx.Osoby.Where(o => o.C_Login == User.Text).FirstOrDefault();
                if (user == null) Response.Redirect("Login.aspx");
                else
                {
                    if (user.C_Haslo != Password.Text) Response.Redirect("Login.aspx");
                    else
                    {
                        Session["User"] = user.IdUzytkownika;

                        //jesli nie bylo parametru back idzie na strone startowa, jesli byl, po zalogowaniu leci na strone wskazana przez Back
                        if (Request.QueryString["back"] == null)
                        {
                            Response.Redirect("/Default");
                        }
                        else
                        {
                            Response.Redirect("/" + Request.QueryString["back"]);
                        }
                    }
                }
            }
        }
    }
}