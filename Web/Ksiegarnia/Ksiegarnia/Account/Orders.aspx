﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="Ksiegarnia.Account.Orders" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="jumbotron">
            <h2> Zamówienie </h2>
        </div>
        <div>
            <asp:GridView ID="OrdersGrid" runat="server" CssClass ="table table-striped" onrowdatabound="OrdersGrid_RowDataBound" >
            </asp:GridView>
        </div>
    </div>
</asp:Content>
