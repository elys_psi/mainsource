﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Panel.aspx.cs" Inherits="Ksiegarnia.Account.Panel" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div>
         <div class="form-horizontal" runat="server" id="RegisterForm">
            <h4>Panel Użytkownika</h4>
            <hr />
            <p>
                <a class="btn btn-default" href="/Account/Orders">Moje zamowienia</a>
            </p>
            <p>
                <a class="btn btn-default" href="/Account/Complaints">Reklamacje</a>
            </p>
        </div>
    </div>
</asp:Content>
