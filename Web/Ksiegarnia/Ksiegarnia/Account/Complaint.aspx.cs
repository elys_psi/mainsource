﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;
using Ksiegarnia.Models.StaticQuerys;

namespace Ksiegarnia.Account
{
    public partial class Complaint : System.Web.UI.Page
    {
        protected void Page_Load()
        {
            var sessionId = Session["User"];
            if(sessionId == null)
            {
                Response.Redirect("/Default");
            }
        }

        protected void Create(object sender, EventArgs e)
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var orderId = Convert.ToInt32(Request.QueryString["orderId"]);
                var complaint = new ReklamacjeZamowien();
                var order = ctx.Zamowienia.FirstOrDefault(o => o.IdZamowienia == orderId);
                if (order != null)
                {
                    complaint.DataRek = DateTime.Now;
                    complaint.IdZamowienia = order.IdZamowienia;
                    complaint.IdUzytkownika = order.IdUzytkownika;
                    complaint.IdMagazyniera = order.IdMagazyniera;
                    complaint.StanRek = "Do rozpatrzenia";
                    complaint.OpisRek = Description.Text;
                    ctx.ReklamacjeZamowien.Add(complaint);
                    ctx.SaveChanges();
                    Response.Redirect("Complaints");
                }
            }
        }
    }
}