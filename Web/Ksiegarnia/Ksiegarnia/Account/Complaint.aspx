﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Complaint.aspx.cs" Inherits="Ksiegarnia.Account.Complaint" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div>
       <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Description" CssClass="col-md-2 control-label">Opis reklamacji</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Description" CssClass="form-control" />
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="Create" Text="Złóż reklamację" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</asp:Content>
