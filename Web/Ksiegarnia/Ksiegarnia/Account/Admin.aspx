﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="Ksiegarnia.Account.Admin" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div>
         <div class="form-horizontal" runat="server" id="RegisterForm">
            <h4>Panel Administratora</h4>
            <hr />
            <p>
                <a class="btn btn-default" href="/Account/Adm/AddCategory">Dodaj kategorię</a>
            </p>
            <p>
                <a class="btn btn-default" href="/Account/Adm/AddAuthor">Dodaj autora</a>
            </p>
            <p>
                <a class="btn btn-default" href="/Account/Adm/AddWyd">Dodaj wydawnictwo</a>
            </p>
            <p>
                <a class="btn btn-default" href="/Account/Adm/AddProduct">Dodaj książkę</a>
            </p>
            <p>
                <a class="btn btn-default" href="/Account/Adm/OrdersView">Zamówienia</a>
            </p>
        </div>
    </div>
</asp:Content>
