﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;
using Ksiegarnia.Models.StaticQuerys;

namespace Ksiegarnia.Account
{
    public partial class Complaints : System.Web.UI.Page
    {
        protected void Page_Load()
        {
            var sessionId = Session["User"];
            
            if (sessionId == null)
            {
                Response.Redirect("/Default");
            }

            var userId = Convert.ToInt32(Session["User"]);
            var id = Rol.Current(Session["User"].ToString());
            //weryfikacja admina
            
            if (id == 2)
            {
                ModeratorView();
            }
            else
            {
                UserView(userId);
            }


        }

        private void ModeratorView()
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var list =
                    from r in ctx.ReklamacjeZamowien
                    join u in ctx.Osoby on r.IdUzytkownika equals u.IdUzytkownika
                    select new { Id = r.IdZamowienia, User = u.C_Login, Date = r.DataRek, Status = r.StanRek, Description = r.OpisRek };

                DataTable dt = new DataTable();


                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Uzytkownik", typeof(string));
                dt.Columns.Add("Opis", typeof(string));
                dt.Columns.Add("Data", typeof(DateTime));
                dt.Columns.Add("Status", typeof(string));
                dt.Columns.Add("Rozpatrz", typeof(string));
                var complaints = list.AsEnumerable();
                foreach (var complaint in complaints)
                {
                    dt.Rows.Add(complaint.Id, complaint.User, complaint.Description, complaint.Date.Value.Date, complaint.Status, "Rozpatrz");
                }
                ComplaintsGrid.DataSource = dt;
                ComplaintsGrid.DataBind();
            }
        }

        private void UserView(int userId)
        {

            using (var ctx = new ksiegarniaEntities())
            {
                var list =
                    from r in ctx.ReklamacjeZamowien
                    join u in ctx.Osoby on r.IdUzytkownika equals u.IdUzytkownika
                    where u.IdUzytkownika == userId
                    select new { Id = r.IdZamowienia, User = u.C_Login, Date = r.DataRek, Status = r.StanRek, Reply = r.DecyzjaRek };

                DataTable dt = new DataTable();


                dt.Columns.Add("Id", typeof(int));
                dt.Columns.Add("Uzytkownik", typeof(string));
                dt.Columns.Add("Data", typeof(DateTime));
                dt.Columns.Add("Status", typeof(string));
                dt.Columns.Add("Decyzja", typeof(string));
                var complaints = list.AsEnumerable();
                foreach (var complaint in complaints)
                {
                    //sprawdzenie czy na pewno jest odpowiedz (zmienia nulla na pusty ciąg, zeby nie wrzucało null reference exception).
                    string reply = complaint.Reply;
                    if (reply == null)
                    {
                        reply = string.Empty;
                    }
                    dt.Rows.Add(complaint.Id, complaint.User, complaint.Date, complaint.Status, reply);
                }
                ComplaintsGrid.DataSource = dt;
                ComplaintsGrid.DataBind();
            }
        }

        protected void ComplaintsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //linkowanie linków do kazdego pola
                HyperLink newHyperLink = new HyperLink();
                newHyperLink.Text = e.Row.Cells[5].Text;
                newHyperLink.NavigateUrl = "/Account/Adm/ReplyComplaint?complaintId=" + e.Row.Cells[0].Text;
                e.Row.Cells[5].Controls.Add(newHyperLink);
            }
        }
    }
}