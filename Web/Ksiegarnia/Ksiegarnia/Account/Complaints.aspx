﻿<%@ Page Title="Admin" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Complaints.aspx.cs" Inherits="Ksiegarnia.Account.Complaints" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="jumbotron">
            <h2> Reklamacje </h2>
        </div>
        <div>
            <asp:GridView ID="ComplaintsGrid" runat="server" CssClass ="table table-striped" onrowdatabound="ComplaintsGrid_RowDataBound" >
            </asp:GridView>
        </div>
    </div>
</asp:Content>
