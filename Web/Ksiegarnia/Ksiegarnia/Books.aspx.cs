﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Web.UI.WebControls;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;

namespace Ksiegarnia
{
    public partial class Books : Page
    {
        public List<Ksiazka> Ksiazki;
        public double promo;

        protected void Page_Load(object sender, EventArgs e)
        {
            var search = Request.QueryString["search"];
            var category = Request.QueryString["category"];
            if (search == null && category == null)
            {
                Ksiazki = GetBooks();
            }
            else if (search != null)
            {
                Ksiazki = SearchBooks(search);
            }
            else if (category != null)
            {
                var categoryId = Convert.ToInt32(category);
                Ksiazki = CategoryBooks(categoryId);
            }
            if(Request.QueryString["Message"] == "Added")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"Dodano książkę do koszyka!\")</SCRIPT>");
            }
            if(Session["User"] != null)
            {
                promo = (100 - GetPromo(Convert.ToInt32(Session["User"])))/100;
            }
            else
            {
                promo = 1;
            }
        }

        public List<Ksiazka> GetBooks()
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var books =
                    from b in ctx.Ksiazki
                    join a in ctx.Autorzy on b.IdAutora equals a.IdAutora
                    select new Ksiazka { ISBN = b.ISBN, Title = b.Tytul, Autor = a.NazwiskoAut, Description = b.OpisKsi, Year = b.RokWydania, WarehouseState = b.StanMagazynowy, Price = b.Cena };
                return books.ToList();
            }
        }

        public List<Ksiazka> SearchBooks(string searchPhrase)
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var books =
                    from b in ctx.Ksiazki
                    join a in ctx.Autorzy on b.IdAutora equals a.IdAutora
                    where b.Tytul.Contains(searchPhrase)
                    select new Ksiazka { ISBN = b.ISBN, Title = b.Tytul, Autor = a.NazwiskoAut, Description = b.OpisKsi, Year = b.RokWydania, WarehouseState = b.StanMagazynowy, Price = b.Cena };
                return books.ToList();
            }
        }

        public List<Ksiazka> CategoryBooks(int categoryId)
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var books =
                    from b in ctx.Ksiazki
                    join a in ctx.Autorzy on b.IdAutora equals a.IdAutora
                    where b.IdKategorii == categoryId
                    select new Ksiazka { ISBN = b.ISBN, Title = b.Tytul, Autor = a.NazwiskoAut, Description = b.OpisKsi, Year = b.RokWydania, WarehouseState = b.StanMagazynowy, Price = b.Cena };
                return books.ToList();
            }
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Books?search=" + SearchBox.Text);
        }

        protected void Unnamed2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Books?category=" + DropDownList1.SelectedValue);
        }

        private double GetPromo(int user)
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var rabaty =
                from o in ctx.Osoby
                join b in ctx.Biblioteki
                on o.IdInstytucji equals b.IdBiblioteki
                where o.IdUzytkownika == user
                select b.RabatyBib.Value;

                return rabaty.FirstOrDefault();

            }
        }

    }
}