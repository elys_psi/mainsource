﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Web.UI.WebControls;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;

namespace Ksiegarnia.Models.StaticQuerys
{
    public static class Rol
    {
        //Pobranie aktualnego numeru sesji
        public static int Current(string id)
        {
            if (id == null)
            {
                return 0;
            }
            else
            {
                var userId = Convert.ToInt32(id);
                using (var ctx = new ksiegarniaEntities())
                {
                    var roleId = ctx.Osoby.First(o => o.IdUzytkownika == userId).IdRoli;
                    return roleId;
                }
            }
        }

        public static string GetName(string id)
        {
            if (id == null)
            {
                return "";
            }
            var userId = Convert.ToInt32(id);
            using (var ctx = new ksiegarniaEntities())
            {
                var login = ctx.Osoby.First(o => o.IdUzytkownika == userId).C_Login;
                return login;
            }
        }

        public static double GetPromo(string id)
        {
            var user = Convert.ToInt32(id);
            using (var ctx = new ksiegarniaEntities())
            {
                var rabaty =
                from o in ctx.Osoby
                join b in ctx.Biblioteki
                on o.IdInstytucji equals b.IdBiblioteki
                where o.IdUzytkownika == user
                select b.RabatyBib.Value;

                return rabaty.FirstOrDefault();
            }
        }
    }
}