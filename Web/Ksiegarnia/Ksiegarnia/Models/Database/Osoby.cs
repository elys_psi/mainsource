//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ksiegarnia.Models.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Osoby
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Osoby()
        {
            this.Oferty = new HashSet<Oferty>();
            this.Oferty1 = new HashSet<Oferty>();
            this.ReklamacjeOfert = new HashSet<ReklamacjeOfert>();
            this.ReklamacjeOfert1 = new HashSet<ReklamacjeOfert>();
            this.ReklamacjeZamowien = new HashSet<ReklamacjeZamowien>();
            this.ReklamacjeZamowien1 = new HashSet<ReklamacjeZamowien>();
            this.Wydawnictwa = new HashSet<Wydawnictwa>();
            this.Zamowienia = new HashSet<Zamowienia>();
            this.Zamowienia1 = new HashSet<Zamowienia>();
        }
    
        public string C_Login { get; set; }
        public string AdresUzyt { get; set; }
        public int IdInstytucji { get; set; }
        public int IdRoli { get; set; }
        public int IdUzytkownika { get; set; }
        public string ImieUzyt { get; set; }
        public string C_Haslo { get; set; }
        public string KodPocztowyUzyt { get; set; }
        public string MailUzyt { get; set; }
        public string Miasto { get; set; }
        public string NazwiskoUzyt { get; set; }
        public string TelefonUzyt { get; set; }
        public Nullable<int> TypInstytucji { get; set; }
        public string UwagiUzyt { get; set; }
        public string Zainteresowania { get; set; }
    
        public virtual Biblioteki Biblioteki { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Oferty> Oferty { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Oferty> Oferty1 { get; set; }
        public virtual Role Role { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReklamacjeOfert> ReklamacjeOfert { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReklamacjeOfert> ReklamacjeOfert1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReklamacjeZamowien> ReklamacjeZamowien { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReklamacjeZamowien> ReklamacjeZamowien1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Wydawnictwa> Wydawnictwa { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Zamowienia> Zamowienia { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Zamowienia> Zamowienia1 { get; set; }
    }
}
