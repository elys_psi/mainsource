//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ksiegarnia.Models.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Autorzy
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Autorzy()
        {
            this.Ksiazki = new HashSet<Ksiazki>();
            this.ZbiorAutorow = new HashSet<ZbiorAutorow>();
        }
    
        public int IdAutora { get; set; }
        public string ImieAut { get; set; }
        public string NazwiskoAut { get; set; }
        public string TytulyNaukowe { get; set; }
        public string UwagiAut { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ksiazki> Ksiazki { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ZbiorAutorow> ZbiorAutorow { get; set; }
    }
}
