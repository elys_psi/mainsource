﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ksiegarnia.Models
{
    public class Ksiazka
    {
        public long Id { get; set; }
        public decimal ISBN { get; set; }
        public string Autor { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? Year { get; set; }
        public int? WarehouseState { get; set; }
        public int? Price { get; set; }
    }
}