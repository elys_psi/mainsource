﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ksiegarnia.Models.Database;

namespace Ksiegarnia
{
    public partial class _Default : Page
    {
        public decimal bookId;
        public string bookName;
        public decimal allBookCount;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var count = from zamowienie in ctx.ZbiorZamowienia
                            group zamowienie.IloscKsiazek by zamowienie.ISBN into toper
                            select new { ISBN = toper.Key, Ilosc = toper.Count() };

                var topISBN = count.OrderByDescending(i => i.Ilosc).FirstOrDefault().ISBN;

                allBookCount = count.Sum(i => i.Ilosc);

                var topBook = ctx.Ksiazki.Where(b => b.ISBN == topISBN).SingleOrDefault();

                if(topBook != null)
                {
                    bookId = topBook.ISBN;
                    bookName = topBook.Tytul;
                }
            }
        }
    }
}