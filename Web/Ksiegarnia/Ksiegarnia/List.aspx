﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="Ksiegarnia.List" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h2>Koszyk</h2>
    </div>
    <div class="row">
       

<%  
    double sum = 0;
    var x = GetList();
    foreach(var book in x)
    {
        sum += (promo * book.Ilosc * book.Price.Value);

            %>
        <div class="col-md-3">
            <h3> <%= HttpUtility.HtmlEncode(book.Title) %> </h3>
            <p>  Autor: <%= HttpUtility.HtmlEncode(book.Autor) %> </p>
            <p>  Rok: <%= HttpUtility.HtmlEncode(book.Year.Value.Year) %> </p>
            <p>  Ilość: <%= HttpUtility.HtmlEncode(book.Ilosc) %> </p>
            <p>  Cena za sztukę: <%= HttpUtility.HtmlEncode(promo * book.Price.Value) %> </p>
            <p>
            <p>
                <a class="btn btn-default" href="List.aspx?delete=<%= HttpUtility.HtmlEncode(book.ISBN) %>">Usuń z koszyka</a>
            </p>
        </div>
        <% } %>
    </div>
        <div class="row">
            <div class ="col-md-12">           
                <p>
                    <a class="btn btn-default" href="Order">Zrealizuj zamówienie</a>
                </p>
            </div>
        </div>
         <div class="row">
            <div class ="col-md-12">  
                    <h3> Suma:  <%= HttpUtility.HtmlEncode(sum) %> zł</h3>
            </div>
        </div>
</asp:Content>
