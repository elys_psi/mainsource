﻿<%@ Page Title="Księgarnia Bartuś" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ksiegarnia._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Księgarnia Bartuś</h1>
        <p class="lead">Witaj w naszej lokalnej księgarni, cieszymy się, że dziś znalazłeś się na naszym stronie. Zapraszamy do zapoznania się z naszą wyjątkową ofertą dla ludzi zafascynowanych czytelnictwem!
                Z pewnością znajdziesz u nas coś dla siebie!</p>
        <p><a href="/Books" class="btn btn-primary btn-lg">Zobacz nasze książki &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Rejestracja</h2>
            <p>
                Aby móc zakupić pozycję z naszego sklepu musisz założyć swoje konto naszego szanownego klienta, serdecznie zapraszamy
            </p>
            <p>
                <a class="btn btn-default" href="/Account/Register">Zarejestruj się &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Top książka</h2>
            <p>
                Najczęsciej kupowaną książką w naszym sklepie jest: <b> <%= HttpUtility.HtmlEncode(bookName) %> </b>
            </p>
            <p>
                <a class="btn btn-default" href="/Book?id=<%=HttpUtility.HtmlEncode(bookId)%>">Przeczytaj o niej! &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Sprzedaż</h2>
            <p>
                Czy wiesz, że nasza biblioteka się stale rozwija? Sprzedajemy naprawdę dużo! Sprzedaliśmy już łącznie <b><%=HttpUtility.HtmlEncode(allBookCount)%> książek</b>
            </p>
            <p>
                <a class="btn btn-default" href="/Books">Kup swoją! &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
