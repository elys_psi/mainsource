﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Web.UI.WebControls;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;
using Ksiegarnia.Models.StaticQuerys;

namespace Ksiegarnia
{
    public partial class List : Page
    {
        public double promo;
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.QueryString["order"] != null) && (Request.QueryString["order"] == "true"))
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"Wysłano zamówienie!\")</SCRIPT>");
            }

            if (Request.QueryString["delete"] != null)
            {
                DeleteBookFromList(Request.QueryString["delete"]);
            }    
            
            if(Session["User"] != null)
            {
                promo = (100 - Rol.GetPromo(Session["User"].ToString())) / 100;
            }       
            else
            {
                promo = 1;
            }
        }

        public List<KoszykElement> GetList()
        {
            var koszykId = Session["IdKoszyka"];
            if (koszykId != null)
            {
                var id = Convert.ToInt32(koszykId);
                using (var ctx = new ksiegarniaEntities())
                {
                    var list =
                        from b in ctx.Ksiazki
                        join k in ctx.Koszyk on b.ISBN equals k.IdKsiazki
                        where k.IdKoszyka == id
                        select new KoszykElement { Id = k.Id, ISBN =b.ISBN, Title = b.Tytul, Description = b.OpisKsi, Year = b.RokWydania, Price = b.Cena, Ilosc = k.Ilosc };
                    return list.ToList();
                }
            }
            else return new List<KoszykElement>();
        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {

        }

        private void DeleteBookFromList(string id)
        {
            var isbn = Convert.ToDecimal(id);
            using (var ctx = new ksiegarniaEntities())
            {
                var koszyk = Session["IdKoszyka"];
                if (koszyk != null)
                {
                    var koszykId = Convert.ToDecimal(koszyk);
                    Koszyk deleter = ctx.Koszyk.Where(x => (x.IdKoszyka == koszykId && x.IdKsiazki == isbn)).FirstOrDefault();
                    if (deleter != null)
                    {
                        ctx.Koszyk.Remove(deleter);
                        ctx.SaveChanges();
                    }
                }
            }
        }
    }
}