﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Web.UI.WebControls;
using System.Data;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;

namespace Ksiegarnia
{
    public partial class Order : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //jesli user nie jest zalogowany idzie do login page i daje mu parametr powrotu na ta strone.
            if(Session["User"] == null)
            {
                Response.Redirect("Account/Login?back=Order");
            }

            var koszykId = Session["IdKoszyka"];
            if (koszykId != null)
            {
                var user = Convert.ToInt32(Session["User"]);
                var id = Convert.ToInt32(koszykId);
                using (var ctx = new ksiegarniaEntities())
                {
                    var list =
                        from b in ctx.Ksiazki
                        join k in ctx.Koszyk on b.ISBN equals k.IdKsiazki
                        join a in ctx.Autorzy on b.IdAutora equals a.IdAutora
                        where k.IdKoszyka == id
                        select new KoszykElement { Id = k.Id, ISBN = b.ISBN, Title = b.Tytul, Description = b.OpisKsi, Year = b.RokWydania, Ilosc = k.Ilosc, Autor = a.NazwiskoAut + " " + a.ImieAut, Price = b.Cena };



                    DataTable dt = new DataTable();
                    dt.Columns.Add("Ksiazka", typeof(string));
                    dt.Columns.Add("Autor", typeof(string));
                    dt.Columns.Add("Opis", typeof(string));
                    dt.Columns.Add("Ilosc", typeof(int));
                    dt.Columns.Add("Cena", typeof(double));
                    OrderGrid.DataSource = dt;
                    var orders = list.AsEnumerable();
                    var rabat = GetPromo(user);
                    foreach (var book in orders)
                    {
                        var price = (100 - rabat) / 100 * book.Price;
                        dt.Rows.Add(book.Title, book.Autor, book.Description, book.Ilosc, price);
                    }
                    OrderGrid.DataBind();
                }
            }
             
        }

        public void CommitOrder(object sender, EventArgs e)
        {
            var koszykId = Session["IdKoszyka"];
            if (koszykId != null)
            {
                var id = Convert.ToInt32(koszykId);
                using (var ctx = new ksiegarniaEntities())
                {
                    var booksToOrder = ctx.Koszyk.Where(k => k.IdKoszyka == id);
                    if (booksToOrder != null)
                    {
                        var user = Convert.ToInt32(Session["User"]);
                        var zamowienie = new Zamowienia();
                        zamowienie.IdZamowienia = ctx.Zamowienia.Max(z => z.IdZamowienia) + 1;
                        zamowienie.DataZam = DateTime.Now;
                        zamowienie.IdUzytkownika = user;
                        zamowienie.Rabat = GetPromo(user);
                        zamowienie.StatusZam = "Zamowione";
                        ctx.Zamowienia.Add(zamowienie);

                        //linkowanie do kazdej ksiazki
                        foreach (var book in booksToOrder)
                        {
                            var bookLink = new ZbiorZamowienia();
                            bookLink.IdZamowienia = zamowienie.IdZamowienia;
                            bookLink.IloscKsiazek = book.Ilosc;
                            bookLink.ISBN = book.IdKsiazki;
                            ctx.ZbiorZamowienia.Add(bookLink);
                            //kod do sprawdzania czy jest na stanie, póki co nie wiem czemu nie działa.
                            //var orderedBook = ctx.Ksiazki.First(o => o.ISBN == book.IdKsiazki);
                            //orderedBook.StanMagazynowy -= book.Ilosc;
                            //if (orderedBook.StanMagazynowy <= 0) throw new Exception("Za dużo zamówionych " + book.IdKsiazki);
                            ctx.Koszyk.Remove(book);
                           
                        }
                    }
                    ctx.SaveChanges();
                    Response.Redirect("/List.aspx?order=true");

                }
            }
        }

        private double GetPromo(int user)
        {
            using (var ctx = new ksiegarniaEntities())
            {
                var rabaty =
                from o in ctx.Osoby
                join b in ctx.Biblioteki
                on o.IdInstytucji equals b.IdBiblioteki
                where o.IdUzytkownika == user
                select b.RabatyBib.Value;

                return rabaty.FirstOrDefault();

            }
        }
    }
}