﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Books.aspx.cs" Inherits="Ksiegarnia.Books" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h2> Lista dostępnych tytułów</h2>

    </div>
    <div class="row">
        <div class="form-group">
            <div class="col-md-offset-1 col-md-2">
                <asp:TextBox runat="server" ID="SearchBox" CssClass="form-control"/>
            </div>
            <div class="col-md-1">
                <asp:Button runat="server" Text="Szukaj" CssClass="btn btn-default" OnClick="Unnamed1_Click" />
            </div>
            <div class="col-md-offset-2 col-md-2">
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="EntityDataSource1" DataTextField="NazwaKat" DataValueField="IdKategorii" CssClass="btn btn-primary dropdown-toggle">
                </asp:DropDownList>
                <asp:EntityDataSource ID="EntityDataSource1" runat="server" ConnectionString="name=ksiegarniaEntities" DefaultContainerName="ksiegarniaEntities" EnableFlattening="False" EntitySetName="Kategorie" EntityTypeFilter="Kategorie">
                </asp:EntityDataSource>
            </div>
            <div class="col-md-1">
                <asp:Button runat="server" Text="Zmień kategorię" CssClass="btn btn-default" OnClick="Unnamed2_Click" />
            </div>

        </div>
    </div>
    
    
    <div class="row">
    <%
        foreach(var book in Ksiazki)
        { 
          
            %>
        <div class="col-md-3">
            <h3> <%= HttpUtility.HtmlEncode(book.Title) %> </h3>
            <p>  Autor: <%= HttpUtility.HtmlEncode(book.Autor) %> </p>
            <p>  Rok: <%= HttpUtility.HtmlEncode(book.Year.Value.Year) %> </p>
            <p>  Cena: <%= HttpUtility.HtmlEncode(promo * book.Price.Value) %> zł </p>
            <p>                
                <a class="btn btn-default" href="Book.aspx?id=<%= HttpUtility.HtmlEncode(book.ISBN) %>">Zobacz więcej &raquo;</a>
            </p>
            <p>                
                <a class="btn btn-default" href="Book.aspx?id=<%= HttpUtility.HtmlEncode(book.ISBN) %>&add=true">Do koszyka!</a>
            </p>
        </div>
        <% } %>
    </div>
</asp:Content>
