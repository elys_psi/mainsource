﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;

namespace Ksiegarnia
{
    public partial class Book : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                string id = Request.QueryString["id"];
                if(Request.QueryString["add"] != null)
                {
                    AddToCard();
                }
            }
            else
            {
                return;
            }
        }

        public List<Ksiazka> GetBook(string id)
        {
            if (id != null)
            {
                using (var ctx = new ksiegarniaEntities())
                {
                    var isbn = Convert.ToDecimal(id);
                    var books =
                        from b in ctx.Ksiazki
                        join a in ctx.Autorzy on b.IdAutora equals a.IdAutora
                        where b.ISBN == isbn
                        select new Ksiazka { ISBN = b.ISBN, Title = b.Tytul, Autor = a.NazwiskoAut, Description = b.OpisKsi, Year = b.RokWydania, Price = b.Cena };
                    return books.ToList();
                }
            }
            else return new List<Ksiazka>();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            AddToCard();
        }

        private void AddToCard()
        {
            if (Session["IdKoszyka"] == null)
            {
                using (var ctx = new ksiegarniaEntities())
                {
                    Session["IdKoszyka"] = ctx.Koszyk.Max(k => k.IdKoszyka) + 1;
                }
            }

            int idKoszyka = Convert.ToInt32(Session["IdKoszyka"]);

            using (var ctx = new ksiegarniaEntities())
            {
                var bookId = Convert.ToDecimal(Request.QueryString["id"]);
                var item = ctx.Koszyk.FirstOrDefault(k => k.IdKoszyka == idKoszyka && k.IdKsiazki == bookId);
                if (item == null)
                {
                    var koszyk = new Koszyk();
                    koszyk.IdKoszyka = idKoszyka;
                    koszyk.IdKsiazki = bookId;
                    koszyk.Ilosc = 1;
                    koszyk.Data = DateTime.Now;
                    var x = ctx.Koszyk.Add(koszyk);
                }
                else
                {
                    item.Ilosc++;
                }
                ctx.SaveChanges();
            }
            Response.Redirect("Books.aspx?message=Added");
        }
    }
}