﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Book.aspx.cs" Inherits="Ksiegarnia.Book" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <% var book = GetBook(Request.QueryString["id"]); %>
    <div class="jumbotron">
        <center>
        <h2> Informacje o książce</h2>
        <p>Tytuł: <%= HttpUtility.HtmlEncode(book[0].Title) %></p>
        <p>Autor: <%= HttpUtility.HtmlEncode(book[0].Autor) %></p>
        <p>Opis: <%= HttpUtility.HtmlEncode(book[0].Description) %></p>
        <p>Rok wydania: <%= HttpUtility.HtmlEncode(book[0].Year.Value.Year) %></p>
        <p>Stan magazynowy: <%= HttpUtility.HtmlEncode(book[0].WarehouseState) %></p>
        <p>Cena: <%= HttpUtility.HtmlEncode(book[0].Price.Value) %> zł</p>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Do koszyka!" />
        </center>
    </div>
</asp:Content>
