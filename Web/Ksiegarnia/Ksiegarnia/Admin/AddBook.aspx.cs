﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using Ksiegarnia.Models;
using Ksiegarnia.Models.Database;

namespace Ksiegarnia.Account
{
    public partial class Register : Page
    {
        protected void AddBook_Click(object sender, EventArgs e)
        {
            //var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            //var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            //var user = new ApplicationUser() { UserName = Email.Text, Email = Email.Text };
            //IdentityResult result = manager.Create(user, Password.Text);
            //if (result.Succeeded)
            //{
            //    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            //    //string code = manager.GenerateEmailConfirmationToken(user.Id);
            //    //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
            //    //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

            //    signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
            //    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);

            //}

            //else
            //{
            //    ErrorMessage.Text = result.Errors.FirstOrDefault();
            //}
            try
            {
                using (var ctx = new KsiegarniaEntities())
                {
                    Ksiazki ksiazka = new Ksiazki()
                    {
                        //ksiazka info
                    };

                    var x = ctx.Ksiazki.Add(ksiazka);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                ErrorMessage.Text = "Wystapil blad przy rejestracji";
            }
            finally
            {
                GoodMessage.Text = "Zarejestrowano pomyslnie!";
                RegisterForm.Visible = false;
            }
        }

        protected void Libraries_Selecting(object sender, System.Web.UI.WebControls.EntityDataSourceSelectingEventArgs e)
        {

        }
    }
}