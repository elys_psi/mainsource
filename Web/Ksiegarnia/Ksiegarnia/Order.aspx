﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="Ksiegarnia.Order" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <h2> Zamówienie </h2>
    </div>
    <div>
        <asp:GridView ID="OrderGrid" runat="server" CssClass ="table table-striped">
            </asp:GridView>
    </div>
    <div class="row">
        <div class ="col-md-12">           
            <p>
                <asp:Button runat="server" Text="Zrealizuj Zamówienie" CssClass="btn btn-default" OnClick="CommitOrder" />
            </p>
        </div>
    </div>
</asp:Content>
